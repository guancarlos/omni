@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="/css/bootstrap-datetimepicker.min.css">
@endsection


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="col-md-12">
                <div class="h4">
                    <a href="{{url('/clients')}}">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                        Regresar a clientes
                    </a>
                </div>
            </div>
            <form action="{{url('/clients')}}" method="post">
                {{ csrf_field() }}
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3 class="text-center">Agregar cliente</h3>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" class="form-control" name="name" value="{{old('name')}}" required/>
                                    @if ($errors->has('name'))
                                        <span class="help-block errors_input">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Correo electronico</label>
                                    <input type="text" class="form-control" name="email" value="{{old('email')}}" required/>
                                    @if ($errors->has('email'))
                                        <span class="help-block errors_input">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Fecha de nacimiento</label>
                                    <input class="form-control" name="birthdate" id="birthdate" value="{{old('birthdate')}}"  required>
                                    @if ($errors->has('birthdate'))
                                        <span class="help-block errors_input">
                                            <strong>{{ $errors->first('birthdate') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Estado</label>
                                    <select class="form-control" name="state" id="state" required/>
                                        <option value="">Seleccionar estado</option>
                                        @foreach($states as $state)
                                            <option value="{{$state->clv_estado}}">{{$state->descripcion}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('state'))
                                    <span class="help-block errors_input">
                                        <strong>{{ $errors->first('state') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Ciudad</label>
                                    <input type="text" class="form-control" name="city" id="city" value="{{old('city')}}" required/>
                                    @if ($errors->has('city'))
                                        <span class="help-block errors_input">
                                            <strong>{{ $errors->first('city') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="submit" class="btn btn-danger pull-right" value="Guardar" />
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript"src="/js/moment.js"></script>
<script type="text/javascript"src="/js/transition.js"></script>
<script type="text/javascript"src="/js/collapse.js"></script>
<script type="text/javascript"src="/js/bootstrap.min.js"></script>
<script type="text/javascript"src="/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript"src="/js/client_form.js"></script>
@endsection
