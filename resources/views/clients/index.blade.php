@extends('layouts.app')

@section('content')
<div class="container">
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Clientes</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{url('/clients/create')}}" class="btn btn-primary pull-right">Agregar</a>
                        </div>
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Fecha de nacimiento</th>
                                        <th>Email</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($clients as $client)
                                    <tr>
                                        <th>{{$client->name}}</th>
                                        <td>{{$client->birthdate}}</td>
                                        <td>{{$client->email}}</td>
                                        <td>
                                            <a href="{{url("clients/$client->id/edit")}}"class="btn btn-primary" >Editar</a>
                                            <button type="button" class="btn btn-danger delete" data-id="{{$client->id}}" data-token="{{ csrf_token() }}">Borrar</button>
                                        </td>
                                    </tr>

                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript"src="/js/clients.js"></script>
@endsection
