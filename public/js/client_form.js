var token = $('[name="csrf-token"]').attr('content');
$('#birthdate').datetimepicker({
    format:'YYYY-MM-DD',
       icons: {
           time: "fa fa-clock-o",
           date: "fa fa-calendar",
           up: "fa fa-arrow-up",
           down: "fa fa-arrow-down",
           previous: "fa fa-arrow-left",
           next: "fa fa-arrow-right"
       }
});

$(document).on('change',('#state',function(){
    var state = $('#state').val();
    if(state){
        $.ajax({
            url: '/cities',
            type: 'post',
            data: {state: state, _token :token},
            success:function(response){
                $.each(json, function(i, obj){
                    $('#select').append($('<option>').text(obj.text).attr('value', obj.val));
                });
            }
        });
    }
}))
