$(document).on('click','.delete',function(){
    var token = $(this).data('token');
    var id  = $(this).data('id');
    $.ajax({
        url: '/clients/'+id,
        type: 'post',
        data: {_method: 'delete', _token :token},
        success:function(msg){
            location.reload();
        }
    });
})
