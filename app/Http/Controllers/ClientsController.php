<?php

namespace App\Http\Controllers;

use SoapClient;
use App\Client;
use Illuminate\Http\Request;
use App\Http\Requests\ClientRequest;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::get();
        return view('clients.index',[
            'clients' => $clients
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $states = $this->states();
        return view('clients.create',[
            'states' => $states
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientRequest $request)
    {
        Client::create($request->all());
        return redirect('/clients')->with('message','Cliente agregado correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Client::find($id)->delete();
    }

    private function states()
    {
        $url = 'http://10.1.10.84:8080/wsa/wsa1/wsdl?targetURI=WSShopping_mxqa';
        $soap = new SoapClient($url);
        $data = [
            'ipc_clv_pais' => 'MEX'
        ];
        $states = $soap->__soapCall("WS_GetEstado",array($data))->opc_respuesta;
        return simplexml_load_string($states);
    }

    public function cities(Request $request)
    {
        $url = 'http://10.1.10.84:8080/wsa/wsa1/wsdl?targetURI=WSShopping_mxqa';
        $soap = new SoapClient($url);
        $data = [
            'ipc_clv_pais' => 'MEX',
            'ipc_clv_estado' => 'AGS',
        ];
        return response()->json([
            'data' => simplexml_load_string($soap->__soapCall("WS_GetCiudad",array($data))->opc_respuesta)
        ]);
        return simplexml_load_string($soap->__soapCall("WS_GetCiudad",array($data))->opc_respuesta);
    }
}
